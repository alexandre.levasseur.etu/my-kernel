#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "mapKeys.h"

char isMaj = 0;

void clear_screen();				/* clear screen */
void putc(char aChar);				/* print a single char on screen */
void puts(char *aString);			/* print a string on the screen */
void puthex(int aNumber);			/* print an Hex number on screen */

void empty_irq(int_regs_t *r) {
}

void press_touche_irq(int_regs_t *r) {
  /*
  puts("Intiruptions niveau 1 appeler\n");
  puts("Hexa : ");
  puthex(_inb(0x60));
  putc('\n');
  puts("Caractere : ");
  putc(_inb(0x60));
  putc('\n');
  */
}


char keyboard_map(unsigned char clef){
    switch (clef) {
        case ecomDown:
            return isMaj?49:'&';
        case eaiguDown:
            return isMaj?50:'e';
        case dquotDown:
            return isMaj?51:'"';
        case squotDown:
            return isMaj?52:'\'';
        case pouvrDown:
            return isMaj?53:'(';
        case minDown:
            return isMaj?54:'-';
        case egraveDown:
            return isMaj?55:'e';
        case undersDown:
            return isMaj?56:'_';
        case ccedDown:
            return isMaj?57:'c';
        case aaiguDown:
            return isMaj?48:'a';
        case pfermDown:
            return isMaj?'\0':')';
        case egalDown:
            return isMaj?'+':'=';

        case backspaceDown:
            return 0x8;

            return '\t';
        case spacebarDown:
            return ' ';

        case aDown:
            return isMaj?'A':'a';
        case bDown:
            return isMaj?'B':'b';

        case dDown:
            return isMaj?'D':'d';
        case eDown:
            return isMaj?'E':'e';
        case fDown:
            return isMaj?'F':'f';
        case gDown:
            return isMaj?'G':'g';
        case hDown:
            return isMaj?'H':'h';
        case iDown:
            return isMaj?'I':'i';
        case jDown:
            return isMaj?'J':'j';
        case kDown:
            return isMaj?'K':'k';
        case lDown:
            return isMaj?'L':'l';
        case mDown:
            return isMaj?'M':'m';
        case nDown:
            return isMaj?'N':'n';
        case oDown:
            return isMaj?'O':'o';
        case pDown:
            return isMaj?'P':'p';
        case qDown:
            return isMaj?'Q':'q';
        case rDown:
            return isMaj?'R':'r';
        case sDown:
            return isMaj?'S':'s';
        case tDown:
            return isMaj?'T':'t';
        case uDown:
            return isMaj?'U':'u';
        case vDown:
            return isMaj?'V':'v';
        case wDown:
            return isMaj?'W':'w';
        case xDown:
            return isMaj?'X':'x';
        case yDown:
            return isMaj?'Y':'y';
        case zDown:
            return isMaj?'Z':'z';

        case shiftgDown:
        case shiftdDown:
            isMaj = 1;
            break;
        case shiftgUp:
        case shiftdUp:
            isMaj = 0;
            break;
        default:
            break;
    }
  return '\0';
}

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{
    /* clear the screen */
    clear_screen();
    puts("Early boot.\n"); 
    puts("\t-> Setting up the GDT... ");
    gdt_init_default();
    puts("done\n");

    puts("\t-> Setting up the IDT... ");
    setup_idt();
    puts("OK\n");
	
    puts("\n\n");

    puts("Yo, hello World !\n"); 
 
    idt_setup_handler(0, empty_irq); 
    idt_setup_handler(1, press_touche_irq); 
 	 
    __asm volatile("sti");

    /* minimal setup done ! */


    unsigned char c;
    while(1){
      if(_inb(0x64) == (0x1D)){
        c = _inb(0x60);
        putc(keyboard_map(c));
      }
    }
}

/* base address for the video output assume to be set as character oriented by the multiboot */
unsigned char *video_memory = (unsigned char *) 0xB8000;

/* clear screen */
void clear_screen() {
  int i;
  for(i=0;i<80*25;i++) { 			/* for each one of the 80 char by 25 lines */
    video_memory[i*2+1]=0x0F;			/* color is set to black background and white char */
    video_memory[i*2]=(unsigned char)' '; 	/* character shown is the space char */
  }
}

/* print a string on the screen */
void puts(char *aString) {
  char *current_char=aString;
  while(*current_char!=0) {
    putc(*current_char++);
  }
}

/* print an number in hexa */
char *hex_digit="0123456789ABCDEF";
void puthex(int aNumber) {
  int i;
  int started=0;
  for(i=28;i>=0;i-=4) {
    int k=(aNumber>>i)&0xF;
    if(k!=0 || started) {
      putc(hex_digit[k]);
      started=1;
    }
  }
}

/* print a char on the screen */
int cursor_x=0;					/* here is the cursor position on X [0..79] */
int cursor_y=0;					/* here is the cursor position on Y [0..24] */

void setCursor() {
  int cursor_offset = cursor_x+cursor_y*80;
  _outb(0x3d4,14);
  _outb(0x3d5,((cursor_offset>>8)&0xFF));
  _outb(0x3d4,15);
  _outb(0x3d5,(cursor_offset&0xFF));
}

void putc(char c) {
  if(cursor_x>79) {
    cursor_x=0;
    cursor_y++;
  }
  if(cursor_y>24) {
    cursor_y=0;
    clear_screen();
  }
  switch(c) {					/* deal with a special char */
    case '\r': cursor_x=0; break;		/* carriage return */
    case '\n': cursor_x=0; cursor_y++; break; 	/* new ligne */	
    case 0x8 : if(cursor_x>0) cursor_x--; break;/* backspace */
    case 0x9 : cursor_x=(cursor_x+8)&~7; break;	/* tabulation */
						/* or print a simple character */
    default  : 
      video_memory[(cursor_x+80*cursor_y)*2]=c;
      cursor_x++;
      break;
  }
  setCursor();
}



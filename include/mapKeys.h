#ifndef _MAPKEYS_H_
#define _MAPKEYS_H_

#define enterDown 0x1C
#define enterUp 0x9C

#define spacebarDown 0x39
#define spacebarUp 0xB9

/*NUMERIC/CARACTERE SPE*/
#define ecomDown 0x2
#define ecomUp 0x82
#define eaiguDown 0x3
#define eaiguUp 0x83
#define dquotDown 0x4
#define dquotUp 0x84
#define squotDown 0x5
#define squotUp 0x85
#define pouvrDown 0x6
#define pouvrUp 0x86
#define minDown 0x7
#define minUp 0x87
#define egraveDown 0x8
#define egraveUp 0x88
#define undersDown 0x9
#define undersUp 0x99
#define ccedDown 0xA
#define ccedUp 0x8A
#define aaiguDown 0xB
#define aaiguUp 0x8B
#define pfermDown 0x1A
#define pfermUp 0x9A
#define egalDown 0xD
#define egalUp 0x8D
#define backspaceDown 0xE
#define backspaceUp 0x8E


/*ALPHA*/
#define aDown 0x10
#define aUp 0x90
#define zDown 0x11
#define zUp 0x91
#define eDown 0x12
#define eUp 0x92
#define rDown 0x13
#define rUp 0x93
#define tDown 0x14
#define tUp 0x94

#define yDown 0x15
#define yUp 0x95
#define uDown 0x16
#define uUp 0x96
#define iDown 0x17
#define iUp 0x97
#define oDown 0x18
#define oUp 0x98
#define pDown 0x19
#define pUp 0x99

#define qDown 0x1E
#define qUp 0x9E
#define sDown 0x1F
#define sUp 0x9F
#define dDown 0x20
#define dUp 0xA0
#define fDown 0x21
#define fUp 0xA1
#define gDown 0x22
#define gUp 0xA2

#define hDown 0x23
#define hUp 0xA3
#define jDown 0x24
#define jUp 0xA4  
#define kDown 0x25
#define kUp 0xA5
#define lDown 0x26
#define lUp 0xA6
#define mDown 0x27
#define mUp 0xA7

#define wDown 0x2C
#define wUp 0xAC
#define xDown 0x2D
#define xUp 0xAD
#define cDown 0x2E
#define cUp 0xAE
#define vDown 0x2F
#define vUp 0xAF
#define bDown 0x30
#define bUp 0xB0

#define nDown 0x31
#define nUp 0xB1


#define shiftgDown 0x2A
#define shiftgUp 0xAA
#define shiftdDown 0x36
#define shiftdUp 0xB6

#endif
